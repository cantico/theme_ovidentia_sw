; <?php/*

[general]
name							="theme_ovidentia_sw"
addon_type						="THEME"
version							="0.3.6"
description						="Ovidentia Skin with horizontal menu without sections"
description.fr					="Skin Ovidentia avec un menu horizontal sans les sections"
long_description.fr             ="README.md"
icon							="icon.png"
image							="thumbnail.png"
delete							=1
addon_access_control			="0"
ov_version						="8.1.97"

author							="Paul de Rosanbo"
encoding						="ISO-8859-15"
mysql_character_set_database	="latin1,utf8"
tags							="theme,default"

[recommendations]
site_sitemap_node				="Custom"

[addons]
jquery							="1.2.6.1"
portlets						="0.8"


[functionalities]
jquery							="Available"


;*/ ?>
