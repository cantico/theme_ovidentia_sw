<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once 'base.php';



function theme_ovidentia_sw_upgrade($version_base, $version_ini)
{
    $addon = bab_getAddonInfosInstance('theme_ovidentia_sw');

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('theme_ovidentia_sw');
        $addon->addEventListener('bab_eventPageRefreshed', 'theme_ovidentia_sw_onPageRefreshed', 'init.php');
    } else {
        bab_addEventListener('bab_eventPageRefreshed', 'theme_ovidentia_sw_onPageRefreshed', $addon->getPhpPath().'init.php', 'theme_ovidentia_sw');
    }

    @bab_functionality::includefile('PortletBackend');
    if (class_exists('Func_PortletBackend')) {
        if(Func_PortletBackend::isEmptyContainer('container-left-locked') && Func_PortletBackend::isEmptyContainer('container-right-locked')){
            Func_PortletBackend::addPortlet('ovml', 'ovml_4', 'container-left-locked', 0, '');//Article home page
            Func_PortletBackend::addPortlet('ovml', 'ovml_2', 'container-left-locked', 1, '');//Last events

            Func_PortletBackend::addPortlet('rssfeed', 'rss_1', 'container-right-locked', 0, 'a:2:{s:6:"number";s:1:"2";s:8:"truncate";s:0:"";}');//RSS ovidentia.org
            Func_PortletBackend::addPortlet('ovml', 'ovml_1', 'container-right-locked', 1, '');//Last articles
            Func_PortletBackend::addPortlet('ovml', 'ovml_3', 'container-right-locked', 2, '');//last files
        }

        if (Func_PortletBackend::isEmptyContainer('theme-sw-header'))
        {
            Func_PortletBackend::addPortlet('ApprobNotify', 'ApprobNotify_0', 'theme-sw-header', 0, '');
        }
    }

    return true;
}

function theme_ovidentia_sw_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('theme_ovidentia_sw');

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('theme_ovidentia_sw');
    } else {
        bab_removeEventListener('bab_eventPageRefreshed', 'theme_ovidentia_sw_onPageRefreshed', $addon->getPhpPath().'init.php');
    }
     return true;
}



function theme_ovidentia_sw_onPageRefreshed() {
    if ('theme_ovidentia_sw' === $GLOBALS['babSkin']) {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();

        if ($icons = @bab_functionality::get('Icons')) {
            $icons->includeCss();
        }
    }
}